package seawarGUIMVC.Controller;

import seawarGUIMVC.Model.Game;
import seawarGUIMVC.View.GameWindow;


public class GameController
{
    // Синглтон
    private static final GameController instance = new GameController();
    private GameController() {} // Пустой конструктор
    public static GameController getInstance()  {   return instance;    }


    public void ShowGameWindow()
    {
        GameWindow.getInstance().ShowGameWindow();
    }


    public String getUserStrike( int y, int x )
    {
        return Game.getInstance().checkUserMove( y , x );
    }


    public String getCompStrike( )
    {
        return Game.getInstance().checkCompMove();
    }


    public String[] getCellsStatus ( String Side )
    {
        return Game.getInstance().getAllCellsStatus( Side );
    }


    public void initNewGame()
    {
        Game.getInstance().resetGame();
        GameWindow.getInstance().resetWindow();
    }
}