package seawarGUIMVC.View;

import seawarGUIMVC.Controller.GameController;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class GameWindow extends JFrame
{
    // Синглтон
    private static final GameWindow instance = new GameWindow();
    public static GameWindow getInstance()      {   return instance;    }
    private GameWindow() {} // Пустой конструктор

    private JPanel CompSide = new JPanel();     // Панель с клетками компьютера
    private JPanel UserSide = new JPanel();     // Панель с клетками пользователя
    private JPanel TextPanel =  new JPanel();   // Панель, содержащая текстовую область с информацией об игре и ходах игроков
    private JTextArea TextArea = new JTextArea(5,100);

    // Сами кнопки
    private JButton[][] CompButtons = new JButton[10][10];
    private JButton[][] UserButtons = new JButton[10][10];

    // Картинки для кнопок
    private ImageIcon emptyWaterICO;
    private ImageIcon liveShipICO;
    private ImageIcon firedShipICO;
    private ImageIcon killedShipICO;
    private ImageIcon emptyShotICO;

    // Кто сейчас ходит: user | comp. При первом запуске программы передаем ход пользователю.
    private String NextStrikeBy = "user";


    // Поток для выполнения стрельбы от пользователя
    private class UserStrikeThread extends Thread
    {
        private int y;
        private int x;

        UserStrikeThread( int y, int x )
        {
            this.y = y;
            this.x = x;
        }

        @Override
        public void run()
        {
            try { Thread.sleep(300); } catch (InterruptedException e) { e.printStackTrace(); } // Пауза для имитации обработки хода
            processingUserStrikeResult( GameController.getInstance().getUserStrike(y, x) );
        }
    }


    // В этом потоке выполняются выстрелы от компьютера
    private class CompStrikeThread extends Thread
    {
        @Override
        public void run()
        {
            try { Thread.sleep(500); } catch (InterruptedException e) { e.printStackTrace(); } // Пауза для имитации обработки хода
            processingCompStrikeResult( GameController.getInstance().getCompStrike() );
        }
    }


    public void ShowGameWindow()
    {
        // Загружаем картинки
        try
        {
            emptyWaterICO = new ImageIcon( ImageIO.read(getClass().getResource("/resources/water.png"))         );
            liveShipICO =   new ImageIcon( ImageIO.read(getClass().getResource("/resources/liveShip.png"))      );
            firedShipICO =  new ImageIcon( ImageIO.read(getClass().getResource("/resources/firedShip.png"))     );
            killedShipICO = new ImageIcon( ImageIO.read(getClass().getResource("/resources/killedShip.png"))    );
            emptyShotICO =  new ImageIcon( ImageIO.read(getClass().getResource("/resources/emptyShot.png"))     );
        }
        catch (Exception ex)
        {
            System.out.println("Невозможно прочитать картинки!");
            ex.printStackTrace();
            System.exit(0);
        }

        // Основные параметры главного окна
        setSize(1200,600); // Утанавливаем размеры окна
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); // Действие по умолчанию при закрытии окна
        setLayout( new BorderLayout() ); // Менеджер компоновки для последующего размещения элементов ( команда выполняется автоматически )
        setTitle("SeaWar");

        CompSide.setLayout( new GridLayout(11, 11) ); // Сетчатый менеджер компоновки
        CompSide.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        UserSide.setLayout( new GridLayout(11, 11) );
        UserSide.setBorder( BorderFactory.createEmptyBorder(10, 10, 10, 10) );

        TextArea.setEditable( false );

        // Устанавливаем автокрокрутку вниз для текстовой области с информацией об игре
        DefaultCaret Caret = (DefaultCaret)TextArea.getCaret();
                     Caret.setUpdatePolicy( DefaultCaret.ALWAYS_UPDATE );

        // Создаем функционал полосы прокрутки
        JScrollPane Scroll = new JScrollPane(TextArea);
                    Scroll.setVerticalScrollBarPolicy( ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS );

        // И размещаем его на текстовой области
        TextPanel.add(Scroll);

        // Заполняем панели кнопками
        fillTheSide( CompSide, CompButtons, true ); // Назначем на кнопки компа слушателей для обработки. Полю юзера они не нужны.
        fillTheSide( UserSide, UserButtons, false );

        // Показываем пользователю его корабли на его панели
        syncCellsStatuses("user", true );

        // Размещаем панели с клетками и текстовую область в главном окне программы
        add( CompSide, BorderLayout.WEST );
        add( UserSide, BorderLayout.EAST );
        add( TextPanel, BorderLayout.NORTH );

        setText(">> Игра началась. Ваш ход.\n");
        setVisible( true );
    }


    private void setText( String Text )  // Обрабатываем поступающий текст
    {
        switch ( Text )
        {
            case "userShooted" :    Text = "Попал! Ваш следующий ход.\n";                               break;
            case "compShooted" :    Text = "Ваш корабль подбит! Следующий ход за компьютером.\n";       break;

            case "killedByUser" :   Text = "Убил! Ваш следующий ход.\n";                                break;
            case "killedByComp" :   Text = "Ваш корабль убит! Следующий ход за компьютером.\n";         break;

            case "userMissedShot" : Text = "Мимо! Ход компьютера...\n";                                 break;
            case "compMissedShot" : Text = "Компьютер промазал! Теперь Ваш ход.\n";                     break;

            case "tryAnotherCell" : Text = "Здесь уже не во что стрелять, выберите другую точку.\n";    break;

            case "youWon" :         Text = "Игра окончена, Вы победили!\n";                             break;
            case "compWon" :        Text = "Игра окончена, компьютер Вас разгромил!\n";                 break;

            case "canNotMoveNow" :  Text = "Сейчас нельзя сделать ход.\n";                              break;

            default : // Оставляем без изменений (для прочих сообщений)
        }
        TextArea.append( Text );
    }


    private void fillTheSide ( JPanel Side, JButton[][] Buttons, boolean createListener )
    {
        // Добавляем в левый верхний угол таблицы решетку и выравниваем ее по центру кнопки
        JLabel zeroPoint = new JLabel( "#" );
               zeroPoint.setHorizontalAlignment( SwingConstants.CENTER );

        Side.add( zeroPoint );

        char columnChar = 'A'; // Буквы для столбцов, начиная с A
        int rowNum = 0; // Номера строк от 0 до 9

        // Заполняем первую из 11 строк буквами (остальные строки уже будут заполняться кнопками)
        for (int i = 0; i < 10; i++)
        {
            JLabel Let = new JLabel( String.valueOf( columnChar ) );
                   Let.setHorizontalAlignment( SwingConstants.CENTER );

            Side.add( Let );
            columnChar++;   // Увеличиваем букву на 1 ( magic chars вроде как )
        }

        // Теперь заполняем столбцы
        for ( int i = 0; i < 10; i++ ) // Для каждой из 10 оставшихся строк
        {
            for ( int j = 0; j < 11; j++ ) // Для каждого из 11 столбцов
            {
                if ( j == 0 ) // j - столбец. В нулевом столбце у нас проставляется маркировка строк от 0 до 9
                {
                    JLabel Let = new JLabel( String.valueOf( rowNum ) ); // Номер ряда
                           Let.setHorizontalAlignment( SwingConstants.CENTER );

                    Side.add( Let );
                    rowNum++;
                }
                else
                {
                    JButton Cell = new JButton();
                            Cell.setBackground( Color.white );
                            Cell.setIcon( emptyWaterICO );
                            Cell.setPreferredSize( new Dimension( 48, 48 ) );

                    Buttons[i][j-1] = Cell; // Уменьшаем номер столбца на 1, т.к. первый столбец заполнен номерами
                    Side.add( Cell );

                    int finalI = i;
                    int finalJ = j-1;

                    if ( createListener )
                    {
                        Cell.addActionListener( new ActionListener() // Назначаем слушатель
                        {
                            @Override
                            public void actionPerformed( ActionEvent e )
                            {
                                if ( "comp".equals( NextStrikeBy ) )
                                    setText("canNotMoveNow");
                                else
                                {
                                    setText("Вы выбрали ячейку: " + "[ X - " + finalI + ", Y - " + finalJ + " ]. ");
                                    new UserStrikeThread(finalI, finalJ).start();
                                    NextStrikeBy = "comp";
                                }
                            }
                        });
                    }
                }
            }
        }
    }


    private void processingUserStrikeResult ( String Result )
    {
        System.out.println(Result);

        if ( !Result.equals("tryAgain") )
            syncCellsStatuses( "comp", false );

        switch ( Result )
        {
            case "shotout":
            {
                setText("userMissedShot");
                new CompStrikeThread().start();
            } break;

            case "killed":      setText( "killedByUser" );      NextStrikeBy = "user";   break;
            case "shooted":     setText( "userShooted" );       NextStrikeBy = "user";   break;
            case "tryAgain":    setText( "tryAnotherCell" );    NextStrikeBy = "user";   break;
            case "defeated":    setText( "youWon" );  showGameOver( "user" );
        }
    }


    private void processingCompStrikeResult( String Result )
    {
        syncCellsStatuses( "user", false );

        switch ( Result )
        {
            case "killed":      setText( "killedByComp" );      break;
            case "shooted":     setText( "compShooted" );       break;
            case "shotout":     setText("compMissedShot");      NextStrikeBy = "user";   break;
            case "defeated":    setText( "compWon" ); showGameOver( "comp" );
        }

        if ( Result.matches("killed|shooted") )
        {
            new CompStrikeThread().start();
        }
    }


    private void syncCellsStatuses ( String Side, boolean showLiveShips )
    {
        String[] Cells = GameController.getInstance().getCellsStatus( Side );

        int counter = 0;

        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                if ( showLiveShips )
                    setCellImage(i, j, Cells[counter], Side );
                else
                {
                    if ( !Cells[counter].equals("ship") )
                        setCellImage(i, j, Cells[counter], Side );
                }
                counter++;
            }
        }
    }


    private void showGameOver( String Winner )
    {
        if ( Winner.equals("comp") )
            syncCellsStatuses( "comp", true ); // Отображаем список живых кораблей компьютера, если таковые остались

        for ( int i = 0; i < 10; i++ )
        {
            for ( int j = 0; j < 10; j++ )
            {
                // Подсвечиваем желтым цветом ячейки с живыми кораблями
                if ( CompButtons[i][j].getIcon().equals(liveShipICO) )
                    CompButtons[i][j].setBackground( Color.YELLOW );

                if ( UserButtons[i][j].getIcon().equals(liveShipICO) )
                    UserButtons[i][j].setBackground( Color.YELLOW) ;

                // Отключаем все кнопки
                CompButtons[i][j].setEnabled(false);
                UserButtons[i][j].setEnabled(false);
            }
        }

        // Создаем и выводим диалоговое окно о завершении игры
        Object[] options = {"Новая игра", "Выход"};
        int n = JOptionPane.showOptionDialog(
                null,
                "Игра завершена",
                "Будем играть снова?",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,      //do not use a custom Icon
                options,        //the titles of buttons
                options[0]);    //default button title

        if ( n == 0 )
        {
            GameController.getInstance().initNewGame();

            if ( "comp".equals( Winner ) )
            {
                setText("______________________\n>> Новая игра! Ход компьютера.\n");
                new CompStrikeThread().start();
            }
            else
            {
                setText("______________________\n>> Новая игра! Ваш ход.\n");
                NextStrikeBy = "user";
            }
        }
        else
            System.exit(0);
    }


    // Установка картинок для кнопок
    private void setCellImage( int y, int x, String Status, String Side )
    {
        JButton[][] Buttons;

        if ( Side.equals ( "comp" ) )
            Buttons = CompButtons;
        else
            Buttons = UserButtons;

        switch ( Status )
        {
            case "empty" :      {   Buttons[y][x].setIcon( emptyWaterICO );     } break;
            case "ship" :       {   Buttons[y][x].setIcon( liveShipICO );       } break;
            case "killed" :     {   Buttons[y][x].setIcon( killedShipICO );     } break;
            case "shooted" :    {   Buttons[y][x].setIcon( firedShipICO );      } break;
            case "shotout" :    {   Buttons[y][x].setIcon( emptyShotICO );      }
        }

        if ( !Status.matches("empty|ship") )
            Buttons[y][x].setBackground( Color.LIGHT_GRAY );  // Подсвечиваем сервым цветом ячейки со всеми статусами, кроме пустого и занятного кораблем
    }


    // Сброс состояния игрового окна до начального (при повторной игре)
    public void resetWindow()
    {
        for ( int i = 0; i < 10; i++ )
        {
            for ( int j = 0; j < 10; j++ )
            {
                CompButtons[i][j].setEnabled( true );
                CompButtons[i][j].setBackground( Color.white );
                CompButtons[i][j].setIcon( emptyWaterICO );

                UserButtons[i][j].setBackground( Color.white );
                UserButtons[i][j].setEnabled( true );
                UserButtons[i][j].setIcon( emptyWaterICO );
            }
        }
        syncCellsStatuses("user", true );
    }
}