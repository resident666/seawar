package seawarGUIMVC.Model;

class Cell
{
    private String status;
    private String shipAccessory;   // Принадлежность ячейки тому или иному кораблю (см. класс Ship)
    private int direction;         // Расположение корабля, которому принадлежит клетка: горизонтальное или вертикальное

    Cell ( )
    {
        this.setStatus("empty");
        this.setShipAccessory("");
        this.direction = -1;
    }

    int getShipDirection()                        {   return this.direction;          }
    String getStatus()                            {   return this.status;             }
    String getShipAccessory()                     {   return this.shipAccessory;      }

    void setShipDirection ( int direction )       {   this.direction = direction;     }
    void setStatus ( String status )              {   this.status = status;           }
    void setShipAccessory(String shipID)          {   this.shipAccessory = shipID;    }
}