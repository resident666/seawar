package seawarGUIMVC.Model;

import java.util.Random;

abstract class Ship // Класс содержит методы для работы с объектами Cell, позволяя расставить их на игровом поле и обозначить принадлежность к условным кораблям
{
    static void spawnShips ( Cell[][] Area )
    {
        createShip( 1, 4, Area ); // 1 корабль из 4 палуб
        createShip( 2, 3, Area ); // 2 корабля по 3 палубы
        createShip( 3, 2, Area ); // ...
        createShip( 4, 1, Area ); // ...

        clearReserved ( Area ); // Очистка клеток от вспомогательного статуса 'reserved' после размещения кораблей
    }


    private static void createShip ( int count, int type, Cell[][] Area ) // Здесь создаем корабли
    {
        Random spawnCoord = new Random();

        for ( int i = 0; i < count; i++ ) // Для каждого из скольки-то кораблей
        {
            int direction =  spawnCoord.nextInt(2); // 0 - увеличиваем по Y, 1 - увеличиваем по X
            int yStart =     spawnCoord.nextInt(10);
            int xStart =     spawnCoord.nextInt(10);

            int shipCells[][] = new int[type][2]; // Создаем массив из количества палуб х 2 координаты для каждой палубы

            shipCells[0][0] = yStart; // Создали координаты для однопалубника (или для первой клетки многопалубника)
            shipCells[0][1] = xStart;

            if ( type > 1 ) // Если это 2-, 3- или 4-палубник
            {
                for ( int j = 1; j < type; j++ ) // Клеток всегда будет меньше на 1, т.к. первая у нас получается рандомным методом выше. Нужно добить оставшиеся
                {
                    if ( direction == 0 )
                    {
                        shipCells[j][0] = yStart + j;                // new Y
                        shipCells[j][1] = xStart;                    // new X
                    }
                    else
                    {
                        shipCells[j][0] = yStart;                    // new Y
                        shipCells[j][1] = xStart + j;                // new X
                    }
                }
            }

            if ( !checkCells (shipCells, Area) ) // Если выбранные клетки того или иного корабля НЕ подходят по условиям размещения, повторяем цикл
                i--;
            else
            {
                // Определяем принадлежность корабля: 40 для 4-палубника, 30, 31 для 3-палубника, 20, 21 и 22 - для 2-палубников и 10, 11, 12 и 13 для одиночных
                String shipAccessory = String.valueOf( type ) + String.valueOf( i );
                // Присваиваем клеткам информацию о кораблях, включая направление
                turnToShip ( shipCells, Area, shipAccessory, direction );
            }
        }
    }


    static int getLargestUserLiveShip( Cell[][] Area )
    {
        String SA = null;
        int largestShipCellsCount = 0;

        for ( int i = 0; i < 10; i++ )
        {
            for ( int j = 0; j < 10; j++ )
            {
                if ( !Area[i][j].getShipAccessory().equals("") && !Area[i][j].getStatus().equals("killed") )
                {
                    SA = Area[i][j].getShipAccessory();

                    String largestSA = String.valueOf(SA).substring( 0, 1 );
                    int currentShipCellsCount = Integer.parseInt( largestSA );

                    if ( currentShipCellsCount > largestShipCellsCount )
                        largestShipCellsCount = currentShipCellsCount;
                }
            }
        }
        return largestShipCellsCount;
    }


    private static void turnToShip ( int[][] shipCells, Cell[][] Area, String shipAccessory, int direction )
    {
        for ( int i = 0; i < shipCells.length; i++ )
        {
            int y = shipCells[i][0];
            int x = shipCells[i][1];

            Area[y][x].setStatus ( "ship" );
            Area[y][x].setShipAccessory ( shipAccessory );
            Area[y][x].setShipDirection ( direction );
        }

        fillAround ( "reserved", shipCells, Area );
        //System.out.println( "Создан корабль : " + shipCells.length + "-палубник");
    }


    private static void clearReserved ( Cell[][] Area )
    {
        for ( int i = 0; i < Area.length; i++ )
        {
            for ( int j = 0; j < Area[i].length; j++ )
            {
                if ( Area[i][j].getStatus().equals("reserved") )
                    Area[i][j].setStatus ( "empty" );
            }
        }
    }


    private static boolean checkCells( int[][] shipCells, Cell[][] Area )
    {
        for ( int i = 0; i < shipCells.length; i++ )
        {
            int y = shipCells[i][0];
            int x = shipCells[i][1];

            if ( y > 9 || x > 9 )
                return false;

            if ( Area[y][x].getStatus().matches ( "ship|reserved" ) )
                return false;
        }
        return true;
    }


    static void fillAround( String marker, int[][] shipCells, Cell[][] Area ) // marker - чем заполняем клетку. Reserved для первичного размещения, killed - после уничтожения корабля противника
    {
        for ( int i = 0; i < shipCells.length; i++ )
        {
            int y = shipCells[i][0];
            int x = shipCells[i][1];

            if ( y != 0 && !Area[y-1][x].getStatus().matches("ship|killed" ) ) Area[y-1][x].setStatus( marker ); // Y: -1, X: 0 // левый
            if ( y != 9 && !Area[y+1][x].getStatus().matches("ship|killed" ) ) Area[y+1][x].setStatus( marker ); // X: +1, Y: 0 // правый

            if ( x != 9 && !Area[y][x+1].getStatus().matches("ship|killed" ) ) Area[y][x+1].setStatus( marker ); // Y: +1, X: 0 // нижний
            if ( x != 0 && !Area[y][x-1].getStatus().matches("ship|killed" ) ) Area[y][x-1].setStatus( marker ); // Y: -1, X: 0 // верхний

            if ( y != 0 && x != 0 && !Area[y-1][x-1].getStatus().matches("ship|killed" ) ) Area[y-1][x-1].setStatus( marker ); // X: -1, Y: -1 // левый верхний угол
            if ( y != 9 && x != 9 && !Area[y+1][x+1].getStatus().matches("ship|killed" ) ) Area[y+1][x+1].setStatus( marker ); // X: +1, Y: +1 // правый нижний угол

            if ( x != 9 && y != 0 && !Area[y-1][x+1].getStatus().matches("ship|killed" ) ) Area[y-1][x+1].setStatus( marker ); // X: +1, Y: -1 // правый верхний угол
            if ( x != 0 && y != 9 && !Area[y+1][x-1].getStatus().matches("ship|killed" ) ) Area[y+1][x-1].setStatus( marker ); // X: -1, Y: +1 // левый нижний угол
        }
    }


    static void fillInside( String marker, int[][] shipCells, Cell[][] Area ) // marker - чем заполняем клетку. Reserved для первичного размещения, killed - после уничтожения корабля противника
    {
        for ( int i = 0; i < shipCells.length; i++ )
        {
            int y = shipCells[i][0];
            int x = shipCells[i][1];
            Area[y][x].setStatus( marker );
        }
    }


    static boolean hasLiveCells( String shipAccessory, Cell[][] Area, String thatCoords )
    {
        for ( int i = 0; i < Area.length; i++ )
        {
            for ( int j = 0; j < Area[i].length; j++ )
            {

                String checkedCoords = String.valueOf( i ) + String.valueOf( j );

                if ( Area[i][j].getShipAccessory().equals(shipAccessory) && !checkedCoords.equals(thatCoords) && Area[i][j].getStatus().equals("ship") )
                    return true;
            }
        }
        return false;
    }


    static int[][] getCellsByAccessory( String shipAccessory, Cell[][] Area )
    {
        String arrLen = String.valueOf(shipAccessory).substring(0,1);
        int arrayLength = Integer.parseInt(arrLen);

        int[][] shipCoords = new int[arrayLength][2];
        short counter = 0;

        for ( int i = 0; i < Area.length; i++ )
        {
            for ( int j = 0; j < Area[i].length; j++ )
            {

                if ( Area[i][j].getShipAccessory().equals(shipAccessory) )
                {
                    shipCoords[counter][0] = i;
                    shipCoords[counter][1] = j;
                    counter++;
                }
            }
        }
        return shipCoords;
    }


    static boolean hasLiveShips ( Cell[][] Area )
    {
        for ( int i = 0; i < Area.length; i++ )
        {
            for ( int j = 0; j < Area[i].length; j++ )
            {
                if ( Area[i][j].getStatus().matches("ship") )
                    return true;
            }
        }
        return false;
    }
}