package seawarGUIMVC.Model;

import seawarGUIMVC.Controller.GameController;
import java.util.*;

public class Game
{
    // Синглтон
    private static final Game instance = new Game();
    private Game() {} // Пустой конструктор
    public static Game getInstance()    {   return instance;    }

    // AI для компьютера : кол-во попаданий по кораблю
    private int lastHitsCounter;

    // Массивы объектов ( ячеек ) игрока и компьютера
    private Cell[][] CompArea;
    private Cell[][] UserArea;


    public void startGame()
    {
        // Установка размеров игровых полей
        CompArea = new Cell[10][10];
        UserArea = new Cell[10][10];

        // Создание ячеек 10 х 10 для игрока и компа
        makeCells( CompArea );
        makeCells( UserArea );

        // Размещение кораблей компьютера и игрока
        Ship.spawnShips( CompArea );
        Ship.spawnShips( UserArea );

        // Отображаем окно программы
        GameController.getInstance().ShowGameWindow();
    }


    // Метод возвращает массив статусов ячеек
    public String[] getAllCellsStatus ( String Side )
    {
        Cell[][] Area;
        String[] Statuses = new String[100];
        int counter = 0;

        if ( "comp".equals(Side ) ) Area = CompArea;
        else Area = UserArea;

        for ( int i = 0; i < 10; i++ )
        {
            for ( int j = 0; j < 10; j++ )
            {
                Statuses[counter] = Area[i][j].getStatus();
                counter++;
            }
        }
        return Statuses;
    }


    private void makeCells ( Cell[][] Area )
    {
        // Создание ячеек User & Comp
        for ( int i = 0; i < Area.length; i++ )
        {
            for ( int j = 0; j < Area[i].length; j++ )
            {
                Area[i][j] = new Cell();
            }
        }
    }


    public String checkUserMove( int y, int x ) // Проверяем пришедшие координаты
    {
        return getStikeResult( y, x, CompArea );
    }


    public String checkCompMove()
    {
        return aiCompMoving();
    }


    private String getStikeResult ( int y, int x, Cell[][] Area )
    {
        switch ( Area[y][x].getStatus() )
        {
            case "ship":
            {
                String shipAccessory = Area[y][x].getShipAccessory();
                String thatCoords = String.valueOf( y ) + String.valueOf( x );

                if ( !Ship.hasLiveCells( shipAccessory, Area, thatCoords ) )
                {
                    int[][] forFilling = Ship.getCellsByAccessory ( shipAccessory, Area );

                    Ship.fillInside("killed", forFilling, Area );
                    Ship.fillAround("shotout", forFilling, Area );

                    if ( !Ship.hasLiveShips( Area ) )
                        return "defeated";
                    else
                        return "killed";
                }
                else
                {
                    Area[y][x].setStatus("shooted");
                    return "shooted";
                }
            }

            case "empty":       Area[y][x].setStatus("shotout");
                                return "shotout";

        }
        return "tryAgain"; // По умолчанию для статусов: "killed", "shotout" и "shooted"
    }

    // Метод выполняет ход компьютера и соответствующие проверки
    private String aiCompMoving ()
    {
        int[] shipCell;

        // Если компьютер ранее попадал по кораблю пользователя, но не убил его
        if (lastHitsCounter > 0)
        {
            // Тогда ищем клетку, ближайщую к подбитой
            shipCell = aiFindNearestCell();
            //System.out.println("Ближайшая к ранее атакованной клетка : " + shipCells[0][0] + shipCells[0][1]);
        }
        else
        {
            // В ином случае узнаем наибольший живой корабль у пользователя и его возможные координаты. Одну (рандомно выбраную) из этих координат атакуем.
            int maxCellsForSearch = Ship.getLargestUserLiveShip( UserArea );
            shipCell = aiGetCellForStrike( maxCellsForSearch, UserArea );
            //System.out.println( "Наибольший живой корабль соперника состоит из " + Ship.getLargestUserLiveShip(UserArea) + " клеток" );
        }

        int y = shipCell[0];
        int x = shipCell[1];
        //System.out.println("Компьютер выбрал клетку : " + y + " " + x + " ");

        String Result = getStikeResult( y, x, UserArea );

        if ( Result.matches("killed|defeated") )
            lastHitsCounter = 0;

        if ( Result.equals("shooted") )
            lastHitsCounter++;

        return  Result;
    }


    // Поиск ближайшей к ранее подбитой компьютером клетке
    private int[] aiFindNearestCell ()
    {
        int[] nearestCell = new int[2];
        List<int[]> shootedCells = new ArrayList<>();

        int direction = -1;
        int counter = 0;

        for ( int i = 0; i < 10; i++ )
        {
            for ( int j = 0; j < 10; j++ )
            {
                if ( UserArea[i][j].getStatus().equals("shooted") )
                {
                    // Добавляем все подбитые клетки в список
                    shootedCells.add( new int[] { i, j } );
                    counter++;

                    // При первом попадании мы не можем определить направление корабля.
                    // Если подбитых клеток 2 или больше, мы можем явно определить его направление ( Это будет 3- или 4-палубник )
                    if ( counter > 1 )
                        direction = UserArea[i][j].getShipDirection();
                }
            }
        }

        // В зависимости от направления, проверяем клетки, близлежащие к подбитым ранее
        switch ( direction )
        {
            case -1 : nearestCell = aiGetPossibleCellBySide( shootedCells, new String[] {"top", "bottom", "left", "right"} ); break;     // Направление не определено
            case  0 : nearestCell = aiGetPossibleCellBySide( shootedCells, new String[] {"top", "bottom"} ); break;                      // Вертикальное
            case  1 : nearestCell = aiGetPossibleCellBySide( shootedCells, new String[] {"left", "right"} );                             // Горизонтальное
        }
        return nearestCell;
    }


    // Поиск подходящих для атаки клеток по направлению (рядом с подбитыми ранее клетками)
    private int[] aiGetPossibleCellBySide ( List<int[]> shootedCells, String[] Sides )
    {
        List<int[]> allPossibleCells = new ArrayList<>();

        for ( int a = 0; a < shootedCells.size(); a++ )
        {
            int[] cellCoords = shootedCells.get( a );
            int y = cellCoords[0];
            int x = cellCoords[1];

            for ( int i = 0; i < Sides.length; i++ )
            {
                String Side = Sides[i];

                if ( "top".equals( Side ) )
                {
                    if (y != 9 && !UserArea[y + 1][x].getStatus().matches("shooted|shotout"))
                        allPossibleCells.add( new int[] { y + 1, x } ); // верхний
                }

                if ( "bottom".equals( Side ) )
                {
                    if (y != 0 && !UserArea[y - 1][x].getStatus().matches("shooted|shotout"))
                        allPossibleCells.add( new int[] { y - 1, x } ); // нижний
                }

                if ( "left".equals( Side ) )
                {
                    if (x != 0 && !UserArea[y][x - 1].getStatus().matches("shooted|shotout"))
                        allPossibleCells.add( new int[] { y, x - 1 } ); // левый
                }

                if ( "right".equals( Side ) )
                {
                    if (x != 9 && !UserArea[y][x + 1].getStatus().matches("shooted|shotout"))
                        allPossibleCells.add( new int[] { y, x + 1 } ); // правый
                }
            }
        }

        // Возвращаем рандомно выбранную клетку из группы возможных
        return allPossibleCells.get( new Random().nextInt( allPossibleCells.size() ) );
    }


    // Ищем клетки по размеру самого большого живого корабля пользователя и возвращаем одну из них
    private int[] aiGetCellForStrike( int maxLiveCells, Cell[][] Area )
    {
        Random spawnCoord = new Random();
        int shipCells[][] = new int[maxLiveCells][2];

        do
        {
            int direction =  spawnCoord.nextInt( 2 ); // 0 - увеличиваем по Y, 1 - увеличиваем по X
            int yStart =     spawnCoord.nextInt( 10 );
            int xStart =     spawnCoord.nextInt( 10 );

            for ( int j = 0; j < maxLiveCells; j++ )
            {
                if ( direction == 0 )
                {
                    shipCells[j][0] = yStart + j;                // new Y
                    shipCells[j][1] = xStart;                    // new X
                }
                else
                {
                    shipCells[j][0] = yStart;                    // new Y
                    shipCells[j][1] = xStart + j;                // new X
                }
            }
        }
        while ( !aiCheckStrikeCells ( shipCells, Area ) );

        int cellNum = spawnCoord.nextInt( maxLiveCells );
        return new int[] { shipCells[cellNum][0], shipCells[cellNum][1] };
    }


    private boolean aiCheckStrikeCells( int[][] shipCells, Cell[][] Area )
    {
        for ( int i = 0; i < shipCells.length; i++ )
        {
            int y = shipCells[i][0];
            int x = shipCells[i][1];

            if ( y > 9 || x > 9 )
                return false;

            if ( Area[y][x].getStatus().matches ( "killed|shooted|shotout" ) )
                return false;
        }
        return true;
    }


    public void resetGame()
    {
        for ( int i = 0; i < 10; i++ )
        {
            for ( int j = 0; j < 10; j++ )
            {
                CompArea[i][j].setStatus("empty");
                UserArea[i][j].setStatus("empty");

                CompArea[i][j].setShipAccessory("");
                UserArea[i][j].setShipAccessory("");
            }
        }

        Ship.spawnShips( CompArea );
        Ship.spawnShips( UserArea );
    }
}